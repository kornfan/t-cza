#!/bin/sh

echo 'LOCAL_PATH := $(call my-dir)'
echo ""
echo 'include $(CLEAR_VARS)'
echo ""
echo '$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)'
echo '$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)'
echo '$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)'
echo ""
echo 'LOCAL_MODULE := cocos2dcpp_shared'
echo ""
echo 'LOCAL_MODULE_FILENAME := libcocos2dcpp'
echo ""

echo "LOCAL_SRC_FILES := hellocpp/main.cpp \\"

while read data; do
	echo "                   ../$data \\"
done

echo ""
echo 'LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes'
echo ""
echo 'LOCAL_WHOLE_STATIC_LIBRARIES := cocos2dx_static cocosdenshion_static cocos_extension_static'
echo ""
echo 'include $(BUILD_SHARED_LIBRARY)'
echo '$(call import-module,.)'

