#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "TeczaPiece.h"
#include "Fireball.h"
#include "SimpleAudioEngine.h"


#include "../extensions/cocos-ext.h"

using namespace cocos2d;
using namespace extension;

class HelloWorld : public cocos2d::Layer
{
private:
    TeczaPiece* pieces[8];
    ParticleBatchNode* _particleBatchNode;
    SpriteBatchNode* _spriteBatchNode;
    EventListenerTouchOneByOne* _listener1;
    EventListenerCustom*    _listener2;
    LabelTTF* czasomierz;
    short _spalone;
    short _min;
    short _sec;
public:
    ~HelloWorld();
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    void pieceHasFallen();
    void fallen();
    void win();
    void shootFireball(float dt);
    void schedShoot(float dt);
    void stopPlaying();
    void replay(Ref *sender, Control::EventType event);
    void quit(Ref *sender, Control::EventType event);
    void lvlUp(float dt);
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    void update_time(float dt);
    std::string path(std::string filename);
    void onKeyReleased(EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
