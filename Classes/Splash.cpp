//
//  Splash.cpp
//  Tecza2
//
//  Created by Festina Lente on 07.11.2014.
//
//

#include "Splash.h"
#include "HelloWorldScene.h"


Scene* Splash::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
    
    // 'layer' is an autorelease object
    auto layer = Splash::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}


bool Splash::init()
{
    if(!Layer::init())
    {
        return false;
    }
    
    auto bg = Sprite::create("res/images/tecza1_start.jpg");
    bg->setPosition(Vec2(getContentSize().width/2, getContentSize().height/2));
    //addChild(bg);
    
    auto bPlay = Sprite::create("res/images/tecza1_graj.png");
    bPlay->setPosition(Vec2(getContentSize().width/2  - 80, getContentSize().height/2 - 240));
    addChild(bPlay,1);
    
    auto bPlay2 = Sprite::create("res/images/tecza1_wyjdz.png");
    bPlay2->setPosition(Vec2(getContentSize().width/2  + 130, getContentSize().height/2 - 240));
    addChild(bPlay2,1);
    
    auto listener0 = EventListenerKeyboard::create();
    listener0->onKeyReleased = CC_CALLBACK_2(Splash::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener0, this);
    
    auto listener1 = EventListenerTouchOneByOne::create();
    listener1->setSwallowTouches(true);
    listener1->onTouchBegan = [&](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
            Director::getInstance()->replaceScene(HelloWorld::createScene());
            return true;
        }
        return false;
    };
    
    auto listener2 = EventListenerTouchOneByOne::create();
    listener2->setSwallowTouches(true);
    listener2->onTouchBegan = [&](Touch* touch, Event* event){
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width, s.height);
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            
            exit(0);
            return true;
        }
        
        return false;
    };
    
    
    auto body = PhysicsBody::createEdgeBox(Size(1024,768), PHYSICSBODY_MATERIAL_DEFAULT, 3);
    body->setCategoryBitmask(01);
    body->setCollisionBitmask(3);
    body->setContactTestBitmask(1);
    body->setDynamic(false);
    auto edgeNode = Node::create();
    edgeNode->setPosition(512,384);
    edgeNode->setPhysicsBody(body);
    this->addChild(edgeNode);
    
    auto car = Node::create();
    
    auto body3 = PhysicsBody::createBox(Size(300,300));
    body3->setDynamic(true);
    body3->setCategoryBitmask(3);
    body3->setCollisionBitmask(1);
    body3->setContactTestBitmask(1);
    car->setPhysicsBody(body3);
    // position the sprite on the center of the screen
    car->setPosition(Vec2(512, 384));
    
    // add the sprite as a child to this layer
    this->addChild(car, 1);
    
    car->setAnchorPoint(Vec2(0.5, 0));
    
    //car->setFlippedX(true);
    
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(Splash::onContactBegin, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener,this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener1->clone(), bPlay);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener2->clone(), bPlay2);
    
    //CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("res/audio/bg3-retro_RT.mp3");
    
    
    
    return true;
}
bool Splash::onContactBegin(cocos2d::PhysicsContact& contact)
{
    CCLOG("onContactBegin -------> ");
    return true;
}
void Splash::onKeyReleased(EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
    exit(0);
}