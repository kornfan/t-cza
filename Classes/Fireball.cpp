//
//  Fireball.cpp
//  Tecza
//
//  Created by Festina Lente on 30.10.2014.
//
//

#include "Fireball.h"

bool Fireball::init()
{
    if(!Sprite::initWithFile(path("pocisk_klatki/pocisk_01.png")))
    {
        return false;
    }
    reversed = false;
    
    setContentSize(Size(200, 500));
    
    Vector<SpriteFrame*> animFrames(4);
    char str[100] = {0};
    for(int i = 1; i < 5; i++)
    {
        sprintf(str, "pocisk_klatki/pocisk_0%d.png",i);
        auto frame = SpriteFrame::create(path(str),Rect(0,0,103,58)); //we assume that the sprites' dimentions are 40*40 rectangles.
        animFrames.pushBack(frame);
    }
    
    auto animation = Animation::createWithSpriteFrames(animFrames, 0.2f);
    animate = Animate::create(animation);
    
    
    _emiter = ParticleSystemQuad::create("res/images/fireball.plist");
    _emiter->setPosition(Vec2(38, 49));
    addChild(_emiter);
    
    return true;
}

void Fireball::shoot(TeczaPiece* target)
{
    this->runAction(RepeatForever::create(animate));
    auto falling = JumpTo::create(2.0, target->getPosition(), 200, 1);
    RotateBy* rotating;
    if (reversed) {
        setRotation(-180);
        rotating = RotateBy::create(2.0, 90);
    }
    else
        rotating = RotateBy::create(2.0, -90);
    //runAction(falling);
    //runAction(rotating);
    auto seq = Sequence::create(falling, CallFunc::create(target, callfunc_selector(TeczaPiece::burn)),CallFunc::create(this, callfunc_selector(Fireball::removeFromParent)),NULL);
    runAction(seq);
    
}
void Fireball::setReversed()
{
    reversed = true;
}
std::string Fireball::path(std::string filename)
{
    std::string temp = "res/images/";
    temp.append(filename);
    return temp;
}