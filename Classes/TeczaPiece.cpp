//
//  TeczaPiece.cpp
//  Tecza
//
//  Created by Festina Lente on 30.10.2014.
//
//

#include "TeczaPiece.h"
#include "SimpleAudioEngine.h"

TeczaPiece* TeczaPiece::create(std::string filename)
{
    TeczaPiece* pRet = new TeczaPiece();
    pRet->initWithFile(filename);
    pRet->autorelease();
    return pRet;
}

bool TeczaPiece::init()
{
    if(!Sprite::init())
    {
        return false;
    }
    _burning = 0;
    _fallen = false;
    return true;
}

void TeczaPiece::burn()
{
    _burning++;
    auto flame = Sprite::create("res/images/tecza_ogien.png");
    Vec2 myPos = getPosition();
    flame->setPosition(rand() % 110 + 50,rand() % 90 + 30);
    flame->setAnchorPoint(Vec2(0.5, 0));
    flame->setScale(cocos2d::random(0.5f, 1.0f));
    flame->runAction(RepeatForever::create(Sequence::create(ScaleBy::create(1.7, 1.4),ScaleBy::create(1.7, 0.72),NULL)));
    
    auto smoke = ParticleSystemQuad::create("res/images/smoke.plist");
    smoke->setPosition(flame->getPosition());
    addChild(smoke);
    
    addChild(flame);
    
    if(_burning > 2)
    {
        fall();
        
    }
}
void TeczaPiece::fall()
{
    if(!_fallen)
    {
        _fallen = true;
        auto fallDown = MoveTo::create((getPosition().y-100)/200, Vec2(getPosition().x, 100));
        auto sequence = Sequence::create(fallDown,CallFunc::create(this, callfunc_selector(TeczaPiece::onGround)), NULL);
        runAction(sequence);
        
        char* buf = new char[10];
        sprintf(buf, "drop");
        
        EventCustom event("game_custom_event1");
        event.setUserData(buf);
        _eventDispatcher->dispatchEvent(&event);
        
        CC_SAFE_DELETE_ARRAY(buf);
    }
}
void TeczaPiece::onGround()
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("res/audio/spadajaca_tecza.mp3");
}

bool TeczaPiece::isRemoved()
{
    return _fallen;
}