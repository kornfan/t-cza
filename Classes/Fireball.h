//
//  Fireball.h
//  Tecza
//
//  Created by Festina Lente on 30.10.2014.
//
//

#ifndef __Tecza__Fireball__
#define __Tecza__Fireball__

#include <stdio.h>
#include "cocos2d.h"
#include "TeczaPiece.h"
using namespace cocos2d;

class Fireball : public Sprite
{
private:
//    Sprite* _fireball;
    ParticleSystemQuad* _emiter;
    Animate* animate;
    bool reversed;
public:
    void setReversed();
    void shoot(TeczaPiece* target);
    std::string path(std::string filename);
    virtual bool init();
    CREATE_FUNC(Fireball);
    //static Fireball* create(bool )
};

#endif /* defined(__Tecza__Fireball__) */
