//
//  TeczaPiece.h
//  Tecza
//
//  Created by Festina Lente on 30.10.2014.
//
//

#ifndef __Tecza__TeczaPiece__
#define __Tecza__TeczaPiece__
#include "cocos2d.h"
#include <stdio.h>

using namespace cocos2d;

class TeczaPiece : public Sprite
{
private:
    short _burning;
    bool _fallen;
public:
    void burn();
    void fall();
    void onGround();
    bool isRemoved();
    static TeczaPiece* create(std::string filename);
    virtual bool init();
    
    CREATE_FUNC(TeczaPiece);
};

#endif /* defined(__Tecza__TeczaPiece__) */
