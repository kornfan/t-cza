//
//  Splash.h
//  Tecza2
//
//  Created by Festina Lente on 07.11.2014.
//
//

#ifndef __Tecza2__Splash__
#define __Tecza2__Splash__

#include <stdio.h>
#include "cocos2d.h"
using namespace cocos2d;

class Splash : public Layer
{
public:
    static Scene* createScene();
    virtual bool init();
    CREATE_FUNC(Splash);
    void onKeyReleased(EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
    bool onContactBegin(cocos2d::PhysicsContact& contact);
};


#endif /* defined(__Tecza2__Splash__) */
