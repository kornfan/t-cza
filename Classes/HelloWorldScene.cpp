#include "HelloWorldScene.h"
#include "Splash.h"
#include "../extensions/cocos-ext.h"
USING_NS_CC;

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define CZCIONKA "fonts/Signika-Regular.ttf"
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define CZCIONKA "fonts/Signika-Regular.ttf"
#endif



Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    _spalone = 0;
    _sec = 30;
    _min = 2;
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    auto bg = LayerColor::create(Color4B(111, 139, 127, 255), visibleSize.width, visibleSize.height);
    addChild(bg);
    
    auto bg3 = Sprite::create(path("tecza_warszawa.png"));
    bg3->setPosition(Vec2(visibleSize.width/2,420));
    addChild(bg3);
    
    auto bg2 = Sprite::create(path("tecza_02_bg.jpg"));
    bg2->setPosition(Vec2(visibleSize.width/2,0));
    bg2->setAnchorPoint(Vec2(0.5,0));
    addChild(bg2);
    
    auto podst1 = Sprite::create(path("tecza_wspornik.jpg"));
    podst1->setAnchorPoint(Vec2(0, 1));
    
    auto podst2 = Sprite::create(path("tecza_wspornik.jpg"));
    podst2->setAnchorPoint(Vec2(1, 1));
    
    auto tecza = Sprite::create(path("tecza_01a.png"));
    tecza->setPosition(Vec2(visibleSize.width/2+25,visibleSize.height/2-27));
    tecza->setScale(0.95);
    addChild(tecza);
    
    czasomierz = LabelTTF::create("2:30", CZCIONKA, 40.0);
    czasomierz->setPosition(Vec2(visibleSize.width-czasomierz->getContentSize().width, visibleSize.height-czasomierz->getContentSize().height));
    czasomierz->setAnchorPoint(Vec2(1, 1));
    addChild(czasomierz);
    
    pieces[0] = TeczaPiece::create(path("tecza_kawalki/tecza_k1.png"));
    pieces[0]->setPosition(Vec2(352, 800-577));
    pieces[0]->setCascadeOpacityEnabled(true);
    addChild(pieces[0],5);
    
    pieces[1] = TeczaPiece::create(path("tecza_kawalki/tecza_k2.png"));
    pieces[1]->setPosition(Vec2(403,800-451));
    pieces[1]->setCascadeOpacityEnabled(true);
    addChild(pieces[1],4);
    
    pieces[2] = TeczaPiece::create(path("tecza_kawalki/tecza_k3.png"));
    pieces[2]->setPosition(Vec2(501, 800 - 351));
    pieces[2]->setCascadeOpacityEnabled(true);
    addChild(pieces[2],3);
    
    pieces[3] = TeczaPiece::create(path("tecza_kawalki/tecza_k4.png"));
    pieces[3]->setPosition(Vec2(643, 800 - 306));
    pieces[3]->setCascadeOpacityEnabled(true);
    addChild(pieces[3],2);
    
    pieces[4] = TeczaPiece::create(path("tecza_kawalki/tecza_k5.png"));
    pieces[4]->setPosition(Vec2(817, 800 - 308));
    pieces[4]->setCascadeOpacityEnabled(true);
    addChild(pieces[4],2);
    
    pieces[5] = TeczaPiece::create(path("tecza_kawalki/tecza_k6.png"));
    pieces[5]->setPosition(Vec2(953, 800 - 371));
    addChild(pieces[5],3);
    
    pieces[6] = TeczaPiece::create(path("tecza_kawalki/tecza_k7.png"));
    pieces[6]->setPosition(Vec2(1057, 800 - 459));
    addChild(pieces[6],4);
    
    pieces[7] = TeczaPiece::create(path("tecza_kawalki/tecza_k8.png"));
    pieces[7]->setPosition(Vec2(1101, 800 - 572));
    addChild(pieces[7],5);
    
    schedule(schedule_selector(HelloWorld::shootFireball),0.6);
    
    podst1->setPosition(Vec2(pieces[0]->getBoundingBox().getMinX()-10,pieces[0]->getBoundingBox().getMinY()));
    addChild(podst1);
    podst2->setPosition(Vec2(pieces[7]->getBoundingBox().getMaxX()+10,pieces[7]->getBoundingBox().getMinY()));
    addChild(podst2);
    
    auto napis = Sprite::create(path("tecza_napis.png"));
    napis->setPosition(Vec2(0, visibleSize.height*0.9));
    napis->setAnchorPoint(Vec2(0, 0.5));
    addChild(napis);
    
    _particleBatchNode = ParticleBatchNode::createWithTexture(nullptr,4000);
    _particleBatchNode->setTexture(TextureCache::getInstance()->addImage("res/images/particleTexture.png"));

    auto listener1 = EventListenerKeyboard::create();
    listener1->onKeyReleased = CC_CALLBACK_2(HelloWorld::onKeyReleased, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener1, this);
    
    _listener1 = EventListenerTouchOneByOne::create();
    _listener1->setSwallowTouches(true);
    
    _listener1->onTouchBegan = [](Touch* touch, Event* event) {
        auto target = static_cast<Sprite*>(event->getCurrentTarget());
        
        //Get the position of the current point relative to the button
        Point locationInNode = target->convertToNodeSpace(touch->getLocation());
        Size s = target->getContentSize();
        Rect rect = Rect(0, 0, s.width*2, s.height*2);
        
        //Check the click area
        if (rect.containsPoint(locationInNode))
        {
            log("sprite began... x = %f, y = %f", locationInNode.x, locationInNode.y);
            target->removeFromParent();
            return true;
        }
        return false;
    };
    
    addChild(_particleBatchNode,5);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("res/audio/hungarian_dance.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("res/audio/policja_ambient.mp3",true);
    
    _listener1->retain();
    
    
    _listener2 = EventListenerCustom::create("game_custom_event1", [=](EventCustom* event){
        _spalone++;
        if(_spalone == 4)
        {
            unschedule(schedule_selector(HelloWorld::shootFireball));
            unschedule(schedule_selector(HelloWorld::update_time));
            
            auto loose = Sprite::create("res/images/plansza_spalona.png");
            loose->setPosition(Vec2(getContentSize().width/2,getContentSize().height/2));
            addChild(loose,50);
            
            
            auto button1 = extension::ControlButton::create();
            button1->setAdjustBackgroundImage(false);
            button1->setPreferredSize(Size(250,100));
            button1->setAnchorPoint(Vec2(0, 0));
            button1->setPosition(Vec2(30, 35));
            button1->addTargetWithActionForControlEvents(this, cccontrol_selector(HelloWorld::replay), Control::EventType::TOUCH_UP_INSIDE);
            loose->addChild(button1);
            
            auto button2 = extension::ControlButton::create();
            button2->setAdjustBackgroundImage(false);
            button2->setPreferredSize(Size(250,100));
            button2->setAnchorPoint(Vec2(0, 0));
            button2->setPosition(Vec2(340, 35));
            button2->addTargetWithActionForControlEvents(this, cccontrol_selector(HelloWorld::quit), Control::EventType::TOUCH_UP_INSIDE);
            loose->addChild(button2);
            
            CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
            CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("res/audio/spalona_tecza.mp3");
        }
    });
    
    
    _eventDispatcher->addEventListenerWithFixedPriority(_listener2->clone(), 1);
    _listener2->retain();
    
    schedule(schedule_selector(HelloWorld::update_time), 1.0);
    scheduleOnce(schedule_selector(HelloWorld::lvlUp), 66.0f);
    
    return true;
}

void HelloWorld::shootFireball(float dt)
{
    int target = rand() % 8;
    while(pieces[target]->isRemoved())
    {
        target = rand() % 8;
        //log("Piece has already fallen");
    }
    auto first = Fireball::create();
    if(target < 4)
    {
        first->setPosition(Vec2(-100, 700));
        first->setReversed();
    }
    else
        first->setPosition(Vec2(1500, 700));
    first->shoot(pieces[target]);
    
   addChild(first,6);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_listener1->clone(),first);
    
}

void HelloWorld::fallen()
{
    unschedule(schedule_selector(HelloWorld::shootFireball));
    unschedule(schedule_selector(HelloWorld::update_time));
    
    auto loose = Sprite::create("res/images/plansza_spalona.png");
    loose->setPosition(Vec2(getContentSize().width/2,getContentSize().height/2));
    addChild(loose,50);
    
    
    auto button1 = extension::ControlButton::create();
    button1->setAdjustBackgroundImage(false);
    button1->setPreferredSize(Size(250,100));
    button1->setAnchorPoint(Vec2(0, 0));
    button1->setPosition(Vec2(30, 35));
    button1->addTargetWithActionForControlEvents(this, cccontrol_selector(HelloWorld::replay), Control::EventType::TOUCH_UP_INSIDE);
    loose->addChild(button1);
    
    auto button2 = extension::ControlButton::create();
    button2->setAdjustBackgroundImage(false);
    button2->setPreferredSize(Size(250,100));
    button2->setAnchorPoint(Vec2(0, 0));
    button2->setPosition(Vec2(340, 35));
    button2->addTargetWithActionForControlEvents(this, cccontrol_selector(HelloWorld::quit), Control::EventType::TOUCH_UP_INSIDE);
    loose->addChild(button2);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("res/audio/spalona_tecza.mp3");
    
}
void HelloWorld::win()
{
    unschedule(schedule_selector(HelloWorld::shootFireball));
    unschedule(schedule_selector(HelloWorld::update_time));
    
    auto win = Sprite::create("res/images/tecza_uratowana.png");
    win->setPosition(Vec2(getContentSize().width/2,getContentSize().height/2));
    addChild(win,50);
    
    auto button1 = extension::ControlButton::create();
    button1->setPreferredSize(Size(250,100));
    button1->setAnchorPoint(Vec2(0, 0));
    button1->setPosition(Vec2(30, 35));
    button1->addTargetWithActionForControlEvents(this, cccontrol_selector(HelloWorld::replay), Control::EventType::TOUCH_UP_INSIDE);
    win->addChild(button1);
    
    auto button2 = extension::ControlButton::create();
    button2->setPreferredSize(Size(250,100));
    button2->setAnchorPoint(Vec2(0, 0));
    button2->setPosition(Vec2(340, 35));
    button2->addTargetWithActionForControlEvents(this, cccontrol_selector(HelloWorld::quit), Control::EventType::TOUCH_UP_INSIDE);
    win->addChild(button2);
    
    CocosDenshion::SimpleAudioEngine::getInstance()->stopBackgroundMusic();
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("res/audio/paradise1.mp3");
}
void HelloWorld::update_time(float dt)
{
    if(_sec == 00)
    {
        if(_min == 0)
        {
            auto call = CallFunc::create(CC_CALLBACK_0(HelloWorld::win, this));
            runAction(call);
        }
        else
        {
            _min--;
            _sec = 60;
        }
    }
    _sec--;
    
    std::stringstream ss;
    ss << _min << ":" << ((_sec < 10) ? "0":"") << _sec;
    czasomierz->setString(ss.str());
}
void HelloWorld::replay(Ref *sender, Control::EventType event)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
    //Director::getInstance()->replaceScene(HelloWorld::createScene());
    Director::getInstance()->replaceScene(Splash::createScene());
    removeFromParentAndCleanup(true);
}
void HelloWorld::quit(cocos2d::Ref *sender, Control::EventType event)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
    Director::getInstance()->replaceScene(Splash::createScene());
}
void HelloWorld::onKeyReleased(EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
    if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE)
    {
        CocosDenshion::SimpleAudioEngine::getInstance()->stopAllEffects();
        Director::getInstance()->replaceScene(Splash::createScene());
    }
}
void HelloWorld::lvlUp(float dt)
{
    unschedule(schedule_selector(HelloWorld::shootFireball));
    schedule(schedule_selector(HelloWorld::shootFireball),0.4);
}
std::string HelloWorld::path(std::string filename)
{
    std::string temp = "res/images/";
    temp.append(filename);
    return temp;
}
HelloWorld::~HelloWorld()
{
    log(">>>>>>>DELETE<<<<<<");
    _eventDispatcher->removeEventListener(_listener2);
}
